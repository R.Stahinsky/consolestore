import exceptions.CustomSqlException;
import menu.Menu;

public class Main {

  public static void main(String[] args) {
    final Menu menu = new Menu();
    try {
      menu.startUp();
      menu.makeChoice();
    } catch (CustomSqlException e) {
      System.out.println(e.getMessage());
    }
  }
}
