package util;

import exceptions.CustomSqlException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import repository.BasketRepository;
import repository.ProductRepository;
import repository.UserRepository;

public class ConnectionUtil {

  private static final String ADD_UUID_EXTENSION = "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";";
  private static final String DB_DRIVER = "org.postgresql.Driver";
  private static final String DB_PASSWORD = "root";
  private static final String DB_USERNAME = "postgres";
  private static final String DB_URL = "jdbc:postgresql://localhost:5432/consolestore";

  private static final UserRepository userRepository = new UserRepository();
  private static final ProductRepository productRepository = new ProductRepository();
  private static final BasketRepository basketRepository = new BasketRepository();

  public static Connection getConnection() {
    try {
      Class.forName(DB_DRIVER);
      connectToDatabase();
      return connectToDatabase();
    } catch (ClassNotFoundException | CustomSqlException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static Connection connectToDatabase() throws CustomSqlException {
    try {
      final Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
      return connection;
    } catch (SQLException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public static void createDB() throws CustomSqlException {
    try {
      addUuidExtension();
      userRepository.createUserTable();
      productRepository.createProductTable();
      basketRepository.createBasketTable();
    } catch (CustomSqlException ex) {
      throw new CustomSqlException(ex.getMessage());
    }
  }

  public static void addUuidExtension() throws CustomSqlException {
    try(final Connection connection = ConnectionUtil.getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(ADD_UUID_EXTENSION);
      preparedStatement.executeUpdate();
      preparedStatement.close();
    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }
}

