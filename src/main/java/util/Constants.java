package util;

import java.util.Scanner;

public class Constants {

  public static final String ENTER = "enter";
  public static final String INVALID_CONSOLE_INPUT = "You have not entered [1], [2], [3], [4], or [q]. Try again.";
  public static final String PRESS = "Press";
  public static final Scanner SCANNER = new Scanner(System.in);
  public static final String STARS = "**************************************";
  public static final String GOODBYE = String.format("%s Goodbye %s", STARS, STARS);
  public static final String WELCOME = String.format(
      "%s \n Welcome to the store \n %s \n Make your choice: \n %s %d %s the store \n %s %d %s the basket \n %s %d %s the profile \n %s %d %s log in/register \n %s [q] to exit\n %s",
      STARS, STARS, PRESS, 1, ENTER, PRESS, 2, ENTER, PRESS, 3, ENTER, PRESS, 4, PRESS, PRESS,
      STARS);
  public static final String AUTH_MENU = String.format(
      "%s Authorization menu %s %n %s %d to log in %n %s %d to register %n %s %s to go back %n %s %s to quit %n %s",
      STARS, STARS, PRESS, 1, PRESS, 2, PRESS, "b", PRESS, "q", STARS);

}
