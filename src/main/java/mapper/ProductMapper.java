package mapper;

import entity.Product;
import exceptions.CustomSqlException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class ProductMapper {

  public final Product mapToEntity(final ResultSet resultSet) throws CustomSqlException {
    try {
      final UUID id = (UUID) resultSet.getObject("id");
      final String name = resultSet.getString("name");
      final Double price = resultSet.getDouble("price");
      final String description = resultSet.getString("description");
      final String additionalInfo = resultSet.getString("additional_info");

      final Product product = new Product(id, name, price, description, additionalInfo);
      return product;

    } catch (SQLException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }
}
