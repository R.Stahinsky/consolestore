package mapper;

import entity.Basket;
import exceptions.CustomSqlException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class BasketMapper {

  public final Basket mapToEntity(final ResultSet resultSet) throws CustomSqlException {
    try {
      final UUID id = (UUID) resultSet.getObject("id");
      final UUID userId = (UUID) resultSet.getObject("user_id");
      final UUID product_id = (UUID) resultSet.getObject("product_id");

      final Basket basket = new Basket(id, userId, product_id);
      return basket;

    } catch (SQLException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }
}
