package mapper;

import entity.User;
import entity.enums.Role;
import exceptions.CustomSqlException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class UserMapper {

  public final User mapToEntity(final ResultSet resultSet) throws CustomSqlException {
    try {
      final UUID id = (UUID) resultSet.getObject("id");
      final String name = resultSet.getString("name");
      final String lastName = resultSet.getString("last_name");
      final String email = resultSet.getString("email");
      final String password = resultSet.getString("password");
      final String additionalInfo = resultSet.getString("additional_info");
      final Role role = (Role) resultSet.getObject("role_state");

      final User user = new User(id, name, lastName, email, password, additionalInfo, role);
      return user;

    } catch (SQLException e){
      throw new CustomSqlException(e.getMessage());
    }
  }
}
