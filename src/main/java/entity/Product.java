package entity;

import java.util.Objects;
import java.util.UUID;

public class Product {

  private UUID id;
  private String name;
  private Double price;
  private String description;
  private String additionalInfo;

  public Product() {
  }

  public Product(final UUID id, final String name, final Double price, final String description, final String additionalInfo) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.description = description;
    this.additionalInfo = additionalInfo;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Product product = (Product) o;
    return Objects.equals(id, product.id) &&
        Objects.equals(name, product.name) &&
        Objects.equals(price, product.price) &&
        Objects.equals(description, product.description) &&
        Objects.equals(additionalInfo, product.additionalInfo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, price, description, additionalInfo);
  }

  @Override
  public String toString() {
    return "Product{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", price=" + price +
        ", description='" + description + '\'' +
        ", additionalInfo='" + additionalInfo + '\'' +
        '}';
  }
}
