package entity;

import java.util.Objects;
import java.util.UUID;

public class Basket {

  private UUID id;
  private UUID userId;
  private UUID productId;

  public Basket() {
  }

  public Basket(final UUID id, final UUID userId, final UUID productId) {
    this.id = id;
    this.userId = userId;
    this.productId = productId;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getUserId() {
    return userId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  public UUID getProductId() {
    return productId;
  }

  public void setProductId(UUID productId) {
    this.productId = productId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Basket basket = (Basket) o;
    return Objects.equals(id, basket.id) && Objects.equals(userId, basket.userId)
        && Objects.equals(productId, basket.productId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, userId, productId);
  }

  @Override
  public String toString() {
    return "Basket{" +
        "id=" + id +
        ", userId=" + userId +
        ", productId=" + productId +
        '}';
  }
}
