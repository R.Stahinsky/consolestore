package entity;

import entity.enums.Role;
import java.util.Objects;
import java.util.UUID;

public class User {

  private UUID id;
  private String name;
  private String lastName;
  private Role role;
  private String email;
  private String password;
  private String additionalInfo;

  public User() {
  }

  public User(final UUID id, final String name, final String lastName, final String email, final String password, final String additionalInfo, final Role role) {
    this.id = id;
    this.name = name;
    this.lastName = lastName;
    this.role = role;
    this.email = email;
    this.password = password;
    this.additionalInfo = additionalInfo;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return role == user.role &&
        Objects.equals(id, user.id) &&
        Objects.equals(name, user.name) &&
        Objects.equals(lastName, user.lastName) &&
        Objects.equals(email, user.email) &&
        Objects.equals(password, user.password) &&
        Objects.equals(additionalInfo, user.additionalInfo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, lastName, role, email, password, additionalInfo);
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", lastName='" + lastName + '\'' +
        ", role=" + role +
        ", email='" + email + '\'' +
        ", password='" + password + '\'' +
        ", additionalInfo='" + additionalInfo + '\'' +
        '}';
  }
}
