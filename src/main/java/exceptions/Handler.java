package exceptions;

public class Handler extends Exception {

  public Handler(String objectToSearch, Integer id) {
    super(objectToSearch + " was not found by id: " + id);
  }
}
