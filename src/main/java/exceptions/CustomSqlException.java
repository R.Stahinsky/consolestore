package exceptions;

import java.sql.SQLException;

public class CustomSqlException extends SQLException {

  public CustomSqlException(String message) {
    super(message);
  }
}
