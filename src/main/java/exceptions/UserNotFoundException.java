package exceptions;

public class UserNotFoundException extends Handler {

  public UserNotFoundException(Integer id) {
    super("User ", id);
  }
}
