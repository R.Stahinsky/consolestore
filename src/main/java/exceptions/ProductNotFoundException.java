package exceptions;

public class ProductNotFoundException extends Handler {

  public ProductNotFoundException(Integer id) {
    super("Product", id);
  }
}
