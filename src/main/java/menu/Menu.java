package menu;

import static util.ConnectionUtil.createDB;
import static util.Constants.GOODBYE;
import static util.Constants.INVALID_CONSOLE_INPUT;
import static util.Constants.SCANNER;
import static util.Constants.WELCOME;
import exceptions.CustomSqlException;

public class Menu {

  public void startUp() throws CustomSqlException {
    try {
      createDB();
    } catch (CustomSqlException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public void makeChoice() throws CustomSqlException {
    System.out.println(WELCOME);

    final String choice = SCANNER.nextLine();

    switch (choice) {
      case "1":
        final Store store = new Store();
        store.goToStore();
        break;
      case "2":
        final Basket basket = new Basket();
        basket.goToBasket();
        break;
      case "3":
        final Profile profile = new Profile();
        profile.goToProfile();
        break;
      case "4":
        final Auth auth = new Auth();
        auth.startAuth();
        break;
      case "q":
        System.out.println(GOODBYE);
        return;
      default:
        System.out.println(INVALID_CONSOLE_INPUT);
        makeChoice();
    }
    SCANNER.close();
  }
}
