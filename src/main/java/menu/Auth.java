package menu;

import static util.Constants.AUTH_MENU;
import static util.Constants.GOODBYE;
import static util.Constants.INVALID_CONSOLE_INPUT;
import static util.Constants.SCANNER;
import static util.Constants.STARS;
import exceptions.CustomSqlException;

public class Auth {

  public void startAuth() throws CustomSqlException {
    System.out.println(AUTH_MENU);

    final String choice = SCANNER.nextLine();

    switch (choice) {
      case "1":
        logIn();
        break;
      case "2":
        register();
        break;
      case "b":
        final Menu menu = new Menu();
        menu.makeChoice();
        break;
      case "q":
        System.out.println(GOODBYE);
        return;
      default:
        System.out.println(INVALID_CONSOLE_INPUT);
        startAuth();
    }
    SCANNER.close();
  }

  private void logIn() {
    System.out.printf("%s %n Enter username: %n", STARS);
    final String userName = SCANNER.nextLine();
    final String password = SCANNER.nextLine();
    SCANNER.close();
  }

  private void register() {
    System.out.printf("%s %n Create username: %n", STARS);
    final String newUserName = SCANNER.nextLine();
    System.out.printf("%s %n Create password: %n ", STARS);
    final String newPassword = SCANNER.nextLine();
    SCANNER.close();
  }
}
