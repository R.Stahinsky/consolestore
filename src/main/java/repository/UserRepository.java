package repository;

import static util.ConnectionUtil.getConnection;
import entity.User;
import entity.enums.Role;
import exceptions.CustomSqlException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import mapper.UserMapper;
import util.ConnectionUtil;

public class UserRepository {

  private final UserMapper userMapper = new UserMapper();

  private static final String INSERT_USER_QUERY = "INSERT INTO store_user(name, last_name, email, password, additional_info, role) VALUES(?,?,?,?,?,?::role_state);";
  private static final String DELETE_USER_QUERY = "DELETE FROM store_user WHERE email = '?';";
  private static final String SELECT_ALL_QUERY = "SELECT * FROM store_user;";
  private static final String SELECT_USER_BY_EMAIL_QUERY = "SELECT * FROM store_user WHERE email = '?';";
  private static final String UPDATE_USER_BY_EMAIL_QUERY = "UPDATE store_user SET name = '?', last_name = '?', email = '?', password = '?', additional_info = '?' WHERE email = '?';";
  private static final String CREATE_USER_TABLE_QUERY =
      "CREATE TYPE role_state AS ENUM ('USER', 'ADMIN');\n"
          + "CREATE TABLE IF NOT EXISTS public.store_user\n"
          + "(\n"
          + "    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),\n"
          + "    name text NOT NULL,\n"
          + "    last_name text NOT NULL,\n"
          + "    email text NOT NULL UNIQUE,\n"
          + "    password text NOT NULL,\n"
          + "    additional_info text,\n"
          + "    role role_state\n"
          + ");\n"
          + "\n"
          + "ALTER TABLE public.store_user\n"
          + "    OWNER to postgres;";

  public void createUserTable() throws CustomSqlException {
    try(final Connection connection = ConnectionUtil.getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER_TABLE_QUERY);
      preparedStatement.executeUpdate();
      preparedStatement.close();
    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public void create(final User user) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_QUERY);
      preparedStatement.setString(1, user.getName());
      preparedStatement.setString(2, user.getLastName());
      preparedStatement.setString(3, user.getEmail());
      preparedStatement.setString(4, user.getPassword());
      preparedStatement.setString(5, user.getAdditionalInfo());
      preparedStatement.setString(6, user.getRole().name());

      preparedStatement.executeUpdate();
      preparedStatement.close();

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public final List<User> getAll() throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_QUERY);
      final ResultSet resultSet = preparedStatement.executeQuery();

      List<User> users = new ArrayList<>();

      while (resultSet.next()) {
        final User foundUser = userMapper.mapToEntity(resultSet);
        users.add(foundUser);
      }
      resultSet.close();
      preparedStatement.close();
      return users;

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public final User getByEmail(final String email) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_EMAIL_QUERY);
      preparedStatement.setString(1, email);

      final ResultSet resultSet = preparedStatement.executeQuery();

      while (resultSet.next()) {
        final UUID userId = (UUID) resultSet.getObject("id");
        final String userName = resultSet.getString("name");
        final String userLastName = resultSet.getString("last_name");
        final String userEmail = resultSet.getString("email");
        final String userPassword = resultSet.getString("password");
        final String userAdditionalInfo = resultSet.getString("additional_info");
        final Role userRole = (Role) resultSet.getObject("role");
        final User foundUser = new User(userId, userName, userLastName, userEmail, userPassword, userAdditionalInfo, userRole);

        resultSet.close();
        preparedStatement.close();

        return foundUser;
      }
    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
    return null;
  }

  public void update(final User user) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_BY_EMAIL_QUERY);
      preparedStatement.setString(1, user.getName());
      preparedStatement.setString(2, user.getLastName());
      preparedStatement.setString(3, user.getEmail());
      preparedStatement.setString(4, user.getPassword());
      preparedStatement.setString(5, user.getAdditionalInfo());

      preparedStatement.setString(6, user.getEmail());
      final int rows = preparedStatement.executeUpdate();
      System.out.println("Update: " + rows);

      preparedStatement.close();

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public void delete(final String email) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_QUERY);
      preparedStatement.setString(1, email);

      final int rows = preparedStatement.executeUpdate();
      System.out.println("Delete:" + rows);

      preparedStatement.close();

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }
}
