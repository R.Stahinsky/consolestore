package repository;

import static util.ConnectionUtil.*;
import static util.ConnectionUtil.getConnection;

import entity.Basket;
import exceptions.CustomSqlException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import mapper.BasketMapper;


public class BasketRepository {

  private final BasketMapper basketMapper = new BasketMapper();

  private static final String INSERT_BASKET_QUERY = "INSERT INTO basket(user_id, product_id) VALUES('?', '?');";
  private static final String DELETE_BASKET_BY_ID = "DELETE FROM basket WHERE id = '?';";
  private static final String SELECT_ALL_QUERY = "SELECT * FROM basket;";
  private static final String SELECT_BASKET_BY_ID = "SELECT * FROM basket WHERE id = '?';";
  private static final String UPDATE_BASKET_BY_ID = "UPDATE basket SET user_id = '?', product_id = '?' WHERE id = '?';";
  private static final String CREATE_BASKET_TABLE_QUERY =
      "CREATE TABLE IF NOT EXISTS public.basket\n"
          + "(\n"
          + "    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),\n"
          + "    user_id uuid NOT NULL CONSTRAINT user_id_fkey REFERENCES store_user (id) ,\n"
          + "    product_id uuid NOT NULL CONSTRAINT product_id_fkey REFERENCES products (id)\n"
          + ");\n"
          + "ALTER TABLE public.basket\n"
          + "    OWNER to postgres;";

  public void createBasketTable() throws CustomSqlException {
    try(final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(CREATE_BASKET_TABLE_QUERY);
      preparedStatement.executeUpdate();
      preparedStatement.close();
    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public void create(final Basket basket) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_BASKET_QUERY);
      preparedStatement.setObject(1, "user_id");
      preparedStatement.setObject(2, "product_id");

      preparedStatement.executeUpdate();
      preparedStatement.close();

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public final Basket getById(final UUID id) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BASKET_BY_ID);
      preparedStatement.setObject(1, id);
      final ResultSet resultSet = preparedStatement.executeQuery();

      Basket foundBasket = null;
      while (resultSet.next()) {
        foundBasket = basketMapper.mapToEntity(resultSet);
      }
      resultSet.close();
      preparedStatement.close();
      return foundBasket;

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public final List<Basket> getAll() throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_QUERY);
      final ResultSet resultSet = preparedStatement.executeQuery();

      List<Basket> baskets = new ArrayList<>();

      while (resultSet.next()) {
        final Basket foundBasket = basketMapper.mapToEntity(resultSet);
        baskets.add(foundBasket);
      }
      resultSet.close();
      preparedStatement.close();
      return baskets;

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public void update(final Basket basket) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BASKET_BY_ID);
      preparedStatement.setObject(1, basket.getUserId());
      preparedStatement.setObject(2, basket.getProductId());

      preparedStatement.setObject(3, basket.getId());
      final int rows = preparedStatement.executeUpdate();
      System.out.println("Updated: " + rows);

      preparedStatement.close();

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public void delete(final UUID id) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BASKET_BY_ID);
      preparedStatement.setObject(1, id);

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }
}
