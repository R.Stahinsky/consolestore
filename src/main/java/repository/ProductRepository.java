package repository;

import static util.ConnectionUtil.getConnection;
import entity.Product;
import exceptions.CustomSqlException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import mapper.ProductMapper;

public class ProductRepository {

  private final ProductMapper productMapper = new ProductMapper();

  private static final String INSERT_PRODUCT_QUERY = "INSERT INTO products(name, price, description, additional_info) VALUES('?', ?, '?', '?');";
  private static final String DELETE_PRODUCT_QUERY = "DELETE FROM products WHERE id ='?';";
  private static final String SELECT_PRODUCT_BY_ID = "SELECT * FROM products WHERE id ='?';";
  private static final String SELECT_ALL_QUERY = "SELECT * FROM products;";
  private static final String UPDATE_PRODUCT_BY_ID = "UPDATE products SET name = '?', price = ?, description = '?', additional_info = '?' WHERE id = '?';";
  private static final String CREATE_PRODUCT_TABLE_QUERY =
      "CREATE TABLE IF NOT EXISTS public.products\n"
          + "(\n"
          + "    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),\n"
          + "    name text NOT NULL,\n"
          + "    price double precision NOT NULL,\n"
          + "    description text NOT NULL,\n"
          + "    additional_info text NOT NULL\n"
          + ");\n"
          + "\n"
          + "ALTER TABLE public.products\n"
          + "    OWNER to postgres;";

  public void createProductTable() throws CustomSqlException {
    try(final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(CREATE_PRODUCT_TABLE_QUERY);
      preparedStatement.executeUpdate();
      preparedStatement.close();
    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public void create(final Product product) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PRODUCT_QUERY);
      preparedStatement.setString(1, product.getName());
      preparedStatement.setDouble(2, product.getPrice());
      preparedStatement.setString(3, product.getDescription());
      preparedStatement.setString(4, product.getAdditionalInfo());

      preparedStatement.executeUpdate();
      preparedStatement.close();

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public final Product getById(final UUID id) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PRODUCT_BY_ID);
      preparedStatement.setObject(1, id);
      final ResultSet resultSet = preparedStatement.executeQuery();

      Product foundProduct = null;
      while (resultSet.next()) {
        foundProduct = productMapper.mapToEntity(resultSet);
      }
      resultSet.close();
      preparedStatement.close();
      return foundProduct;

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public final List<Product> getAll() throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_QUERY);
      final ResultSet resultSet = preparedStatement.executeQuery();

      List<Product> products = new ArrayList<>();

      while (resultSet.next()) {
        final Product foundProduct = productMapper.mapToEntity(resultSet);
        products.add(foundProduct);
      }
      resultSet.close();
      preparedStatement.close();
      return products;

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public void update(final Product product) throws CustomSqlException {
    try (final Connection connection = getConnection()) {

      final PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PRODUCT_BY_ID);
      preparedStatement.setString(1, product.getName());
      preparedStatement.setDouble(2, product.getPrice());
      preparedStatement.setString(3, product.getDescription());
      preparedStatement.setString(4, product.getAdditionalInfo());

      preparedStatement.setObject(5, product.getId());

      preparedStatement.close();

      final int rows = preparedStatement.executeUpdate();
      System.out.println("Update: " + rows);

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }

  public void delete(final UUID id) throws CustomSqlException {
    try (final Connection connection = getConnection()) {
      final PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PRODUCT_QUERY);
      preparedStatement.setObject(1, id);

      final int rows = preparedStatement.executeUpdate();
      System.out.println("Delete: " + rows);

      preparedStatement.close();

    } catch (SQLException | NullPointerException e) {
      throw new CustomSqlException(e.getMessage());
    }
  }
}
